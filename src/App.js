import React, { Fragment } from 'react';
import { Route, Switch } from "react-router-dom";
import { Link } from "react-router-dom";

import Home from "./Home";
import Details from "./Details";
import BkCategory from "./Admin/Category/Home";
import BkListing from "./Admin/Listing/Home";

function App() {
    return (
        <Fragment>
            <div className="container">
                <nav className="navbar navbar-expand-lg navbar-light bg-light">
                    <Link className="navbar-brand" to="/">Directory</Link>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarNav">
                        <ul className="navbar-nav">
                            <li className="nav-item">
                                <Link className="nav-link" to="/">Home</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/admin/listing">Manage Listing</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/admin/category">Manage Category</Link>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>

            <div className="container py-5">
                <Switch>
                    <Route exact path="/" component={Home} />
                    <Route exact path='/:slug' component={Details}></Route>
                    <Route path="/admin/category" component={BkCategory} />
                    <Route path="/admin/listing" component={BkListing} />
                </Switch>
            </div>
        </Fragment>
    )
}

export default App;
