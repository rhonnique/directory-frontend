import React, { Component } from "react";
import axios from "axios";
import * as Toastr from 'toastr';
import $ from "jquery";

const AppContext = React.createContext();

//Toastr.options.showDuration = 300;
Toastr.options.timeout = 5000;
Toastr.options.extendedTimeOut = 5000;
Toastr.options.closeButton = true;

const Axios = axios.create();
const AxiosAuth = axios.create();


export class AppContextProvider extends Component {
    constructor(props) {
        super(props)

        this.state = {
            title: 'Directory',
            api_url: 'http://localhost:8888',
            image_url: 'http://localhost:8888/uploads',
            //api_url: 'http://173.230.149.104:3000',
            //image_url: 'http://173.230.149.104:3000',
            settings: {}
        }
    }

    componentWillMount() {
        axios.defaults.baseURL = this.state.api_url;
        AxiosAuth.interceptors.request.use((config) => {
            const token = localStorage.getItem("token");
            if (token) {
                config.headers.Authorization = `Bearer ${token}`;
            }
            return config;
        })

        Axios.interceptors.request.use((config) => {
            const token = localStorage.getItem("token");
            if (token) {
                config.headers.Authorization = `Bearer ${token}`;
            }
            return config;
        })

        // AxiosAuth.interceptors.response.use((response) => response, this.handleAxiosError);

    }

    setTitle = (title = null) => {
        document.title = (title ? title + ' - ' : '') + this.state.title;
    }

    /*********
     *  ******* BACKEND CATEGORY *******
     ****************************/

    bkCategoryGet = async () => {
        const response = await AxiosAuth.get("/api/backend/category");
        return response;
    }

    bkCategoryAdd = async (data) => {
        const response = await AxiosAuth.post("/api/backend/category", data);
        return response;
    }

    bkCategoryEdit = async (id, data) => {
        const response = await AxiosAuth.put(`/api/backend/category/${id}`, data);
        return response;
    }

    bkCategoryDelete = async (id) => {
        const response = await AxiosAuth.delete(`/api/backend/category/${id}`);
        return response;
    }

    categoryGet = async () => {
        const response = await Axios.get("/api/backend/category/get");
        return response;
    }


    /*********
     *  ******* BACKEND LISTING *******
     ****************************/

    bkListingGet = async () => {
        const response = await AxiosAuth.get("/api/backend/listing");
        return response;
    }

    bkListingAdd = async (data) => {
        const response = await AxiosAuth.post("/api/backend/listing", data);
        return response;
    }

    bkListingEdit = async (id, data) => {
        const response = await AxiosAuth.put(`/api/backend/listing/${id}`, data);
        return response;
    }

    bkListingDelete = async (id) => {
        const response = await AxiosAuth.delete(`/api/backend/listing/${id}`);
        return response;
    }



     /*********
     *  ******* LISTING *******
     ****************************/

    listingGet = async () => {
        const response = await AxiosAuth.get("/api/listings");
        return response;
    }

    listingShow = async (slug) => {
        const response = await AxiosAuth.get(`/api/listings/${slug}`);
        return response;
    }

    errorHandle = error => {
        const errMsg = error.response && error.response.data && error.response.data.message;
        const msg = errMsg ? errMsg : "An error occurred, please retry!";
        this.setToastr("Error Notification!", msg, "error");
    }

    setToastr = (title, msg, type = 'info') => {
        Toastr[type](msg, title);
    }


    render() {
        return (
            <AppContext.Provider
                value={{
                    setTitle: this.setTitle,
                    setToastr: this.setToastr,
                    errorHandle: this.errorHandle,

                    bkCategoryGet: this.bkCategoryGet,
                    bkCategoryAdd: this.bkCategoryAdd,
                    bkCategoryEdit: this.bkCategoryEdit,
                    bkCategoryDelete: this.bkCategoryDelete,
                    categoryGet: this.categoryGet,

                    bkListingGet: this.bkListingGet,
                    bkListingAdd: this.bkListingAdd,
                    bkListingEdit: this.bkListingEdit,
                    bkListingDelete: this.bkListingDelete,

                    listingGet: this.listingGet,
                    listingShow: this.listingShow,

                    ...this.state
                }}
            >

                {this.props.children}

            </AppContext.Provider>
        )
    }
}

export const withContext = Component => {
    return props => {
        return (
            <AppContext.Consumer>
                {
                    globalState => {
                        return (
                            <Component
                                {...globalState}
                                {...props}
                            />
                        )
                    }
                }
            </AppContext.Consumer>
        )
    }
}
