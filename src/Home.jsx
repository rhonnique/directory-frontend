import React, { useState, useEffect, useRef } from "react";
import { withContext } from "./AppContext";
import List from "./List";
import { NavLink } from "react-router-dom";

const Home = props => {
  const { history, image_url } = props;

  const [data, setData] = useState([]);
  const [dataLoader, setDataLoader] = useState(false);

  const fetchData = async () => {
    setDataLoader(dt => ({ ...dt, loading: true, error: false, rd: false }));
    props
      .listingGet()
      .then(response => {
        setData(response.data);
        setDataLoader(dt => ({
          ...dt,
          loading: false,
          error: false,
          rd: true
        }));
      })
      .catch(err => {
        setDataLoader(dt => ({
          ...dt,
          loading: false,
          error: true,
          rd: false
        }));
      });
  };

  useEffect(() => {
    props.setTitle();
    fetchData();
  }, []);

  return (
    <React.Fragment>
      {dataLoader.loading && <div>Loading ...</div>}
      {dataLoader.error && <div>an error occured</div>}
      {dataLoader.rd && data.length === 0 && <div>No result found!</div>}

      {dataLoader.rd && data.length > 0 && (
        <div className="row">
          {data.map((row, index) => {
              return (
                <List
                  key={index}
                  list={row}
                />
              );
            })}
        </div>
      )}
      

        

        
      
    </React.Fragment>
  );
};

export default withContext(Home);
