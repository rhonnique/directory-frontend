import React from "react";
import { NavLink } from 'react-router-dom';
import { withRouter } from "react-router";
import { withContext } from "./AppContext";

const List = ({ list, image_url }) => {
  const desc = list.description;
  return (
    <div className="col-sm-3" style={{ marginBottom: '2rem' }}>
      <div className="card">
      <NavLink to={`/${list.slug}`}><img src={`${image_url}/${list.images[0]}`} className="card-img-top" /></NavLink>
        <div className="card-body">
          <h6 className="card-title">
            <NavLink to={`/${list.slug}`}>{list.business_name}</NavLink>
          </h6>

          <p className="card-text">{desc.substring(0, 50)}...</p>
          <NavLink to={`/${list.slug}`}>View details</NavLink>
        </div>
      </div>
    </div>
  );
}

export default withRouter(withContext(List));