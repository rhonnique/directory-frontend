import React, { useState, useEffect, useRef } from "react";
import { withContext } from "./AppContext";

const Details = props => {
  const { history, image_url, match } = props;

  const [slug, setSlug] = useState(match.params.slug);
  const [data, setData] = useState([]);
  const [dataLoader, setDataLoader] = useState(false);
  const [categories, setCategories] = useState([]);

  const fetchData = async () => {
    setDataLoader(dt => ({ ...dt, loading: true, error: false, rd: false }));
    props
      .listingShow(slug)
      .then(response => {
        const details = response.data;
        setData(details);
        if(details){
          setCategories(details.categories.map(c => c.category.name));
        }

        setDataLoader(dt => ({
          ...dt,
          loading: false,
          error: false,
          rd: true
        }));
      })
      .catch(err => {
        setDataLoader(dt => ({
          ...dt,
          loading: false,
          error: true,
          rd: false
        }));
      });
  };

  useEffect(() => {
    props.setTitle();
    fetchData();
  }, []);

  return (
    <React.Fragment>
      {dataLoader.loading && <div>Loading ...</div>}
      {dataLoader.error && <div>an error occured</div>}
      {dataLoader.rd && data === null && <div>No result found!</div>}

      {dataLoader.rd && data !== null && (
        <div>
          <div class="row">
            <div class="col-4">
              {data.images.map((image, index) => (
                <img key={index}
                style={{width: '100%', marginBottom: '15px'}}
                  class="img-fluid"
                  src={`${image_url}/${image}`}
                />
              ))}
            </div>

            <div class="col-8">
              <h3>{data.business_name}</h3>
              <p>Categories: {categories.join(", ")}</p>
              <p>{data.description}</p>
              <h6>Business Details</h6>
              <ul>
                <li>{data.address}</li>
                <li>{data.phone}</li>
                <li>{data.email}</li>
                {data.website && <li>{data.website}</li>}
              </ul>
            </div>
          </div>

          <hr />
        </div>
      )}
    </React.Fragment>
  );
};

export default withContext(Details);
