import React, { Fragment } from "react";

const TableRow = ({ row, handleOpenEdit, handleDeletedata }) => {
    const categories = row.categories.map(c => c.category.name)
    return (
        <tr>

            <td>{row.business_name}</td>
            <td>{categories.join(", ")}</td>
            <td className="text-center">{row.views}</td>
            <td className="text-center">
                <a onClick={() => handleOpenEdit(row)} href="javascript:void(0)" data-toggle="modal" data-target="#modalForm">Edit</a> | <a onClick={() => handleDeletedata(row._id)} href="javascript:void(0)">Delete</a>
            </td>
        </tr>

    );
}

export default TableRow;