import React, { useState, useRef, Fragment, useEffect } from "react";
import { FormWithConstraints, FieldFeedbacks, FieldFeedback, Async as Async_, AsyncProps } from 'react-form-with-constraints';
import { withContext } from "../../AppContext";


const Edit = props => {
    const form = useRef(null);
    const { handleSubmitEdit, cats, catsLoading, catsError, clearFormData, editData, image_url } = props;
    const [selectCats, setSelectCats] = useState(editData.categories.map((c) => c.category._id));
    
    const [values, setValues] = useState(editData);
    const [currentImages, setCurrentImages] = useState(editData.images);

    const handleChange = (e) => {
        const { name, value } = e.target
        form.current.validateFields(e.target);
        setValues({ ...values, [name]: value })
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        let target = e.target.elements;
        await form.current.validateFields();

        if (form.current.isValid()) {
            handleSubmitEdit(target, document.getElementsByName('selected_cats'), currentImages, values._id);
        }
    }

    const removeImg = index => {
        const newImg = [...currentImages];
        newImg.splice(index, 1);
        setCurrentImages(newImg);
    };

    return (
        <Fragment>
            <FormWithConstraints id="form" ref={form} onSubmit={handleSubmit} noValidate>
                <div className="modal-body">

                    <div className="form-group">
                        <label>Business Name</label>
                        <input type="text" className="form-control"
                            onChange={handleChange} value={values.business_name}
                            name="business_name" placeholder="Business Name" required
                        />

                        <FieldFeedbacks for="name">
                            <FieldFeedback when="*" />
                        </FieldFeedbacks>
                    </div>


                    <div className="form-group">
                        <label>Categories</label>
                        {catsLoading && (<div>..loading categories</div>)}
                        {catsError && (<div>Can not load categories</div>)}
                        {cats && (
                            <div className="row">
                                {cats.map((cat, index) => {
                                    return (<div key={index} className="col-sm-6">
                                        <div className="checkbox-custom checkbox-primary checkbox-inline">
                                            <input name="selected_cats"
                                                defaultChecked={selectCats.includes(cat._id)}
                                                value={cat._id} type="checkbox" id={'cat_' + cat._id} />
                                            <label htmlFor={'cat_' + cat._id}>{cat.name}</label>
                                        </div>
                                    </div>)
                                })}
                                <input style={{ display: 'none' }} type="text" name="cats" />
                            </div>

                        )}

                        <FieldFeedbacks for="cats">
                            <FieldFeedback when="*" >Select at least one option</FieldFeedback>
                        </FieldFeedbacks>
                    </div>

                    <hr />



                    <div className="row">
                        <div className="col-sm-6">
                            <div className="form-group">
                                <label>Contact Phone</label>
                                <input type="text" className="form-control"
                                    onChange={handleChange}
                                    value={values.phone}
                                    name="phone" placeholder="Contact Phone" required
                                />

                                <FieldFeedbacks for="name">
                                    <FieldFeedback when="*" />
                                </FieldFeedbacks>
                            </div>
                        </div>
                        <div className="col-sm-6">
                            <div className="form-group">
                                <label>Email Address</label>
                                <input type="text" className="form-control"
                                    onChange={handleChange} value={values.email}
                                    name="email" placeholder="Email Address" required
                                />

                                <FieldFeedbacks for="name">
                                    <FieldFeedback when="*" />
                                </FieldFeedbacks>
                            </div>
                        </div>
                    </div>

                    <div className="form-group">
                        <label>Address</label>
                        <textarea className="form-control"
                            onChange={handleChange} rows="2"
                            value={values.address}
                            name="address" placeholder="Address"></textarea>
                    </div>

                    <div className="form-group">
                        <label>Website</label>
                        <input type="text" className="form-control"
                            onChange={handleChange} value={values.website}
                            name="website" placeholder="Website" required
                        />

                        <FieldFeedbacks for="name">
                            <FieldFeedback when="*" />
                        </FieldFeedbacks>
                    </div>

                    <div className="form-group">
                        <label>Description</label>
                        <textarea className="form-control"
                            onChange={handleChange} rows="4"
                            value={values.description}
                            name="description" placeholder="Description"></textarea>
                    </div>

                    <div className="form-group">
                        <label>Upload Images</label>
                        
                            <ul className="bk-list-img">
                                {currentImages.map((img, index) => {
                                    return (<li key={index}>
                                        <span className="remove" onClick={() => removeImg(index)}>x</span>
                                        <img src={`${image_url}/${img}`} />
                                    </li>)
                                })}
                            </ul>
                        
                        <input type="file" className="form-control" onChange={handleChange}
                        name="images" multiple />
                    </div>


                </div>
                <div className="modal-footer">
                    <button className="btn btn-primary" type="submit">Save</button>
                    <a className="btn btn-sm btn-white" id="close-modal" 
                    onClick={clearFormData}
                        data-dismiss="modal" href="javascript:void(0)">Cancel</a>
                </div>
            </FormWithConstraints>
        </Fragment>
    );
}

export default withContext(Edit);