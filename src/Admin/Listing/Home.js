import React, { Fragment, useState, useEffect } from "react";
import { withContext } from "../../AppContext";
import TableRow from "../Listing/TableRow";
import Add from "./Add";
import Edit from "./Edit";

var pluralize = require("pluralize");

const Home = props => {
  const { api_url } = props;
  const title = `Listing`;
  const api_path = `listing`;

  const [data, setData] = useState([]);
  const [dataLoader, setDataLoader] = useState(false);

  const [cats, setCats] = useState([]);
  const [catsLoading, setCatsLoading] = useState(false);
  const [catsError, setCatsError] = useState(false);

  const [actionLoader, setActionLoader] = useState({});
  const [actionLoading, setActionLoading] = useState(false);
  const [actionError, setActionError] = useState(false);

  const [modalTitle, setModalTitle] = useState("");
  const [editMode, setEditMode] = useState(false);
  const [editData, setEditData] = useState({});

  // Get main data
  const fetchData = async () => {
    setDataLoader(dt => ({ ...dt, loading: true, error: false, rd: false }));
    props
      .bkListingGet()
      .then(response => {
        setData(response.data);
        setDataLoader(dt => ({
          ...dt,
          loading: false,
          error: false,
          rd: true
        }));
      })
      .catch(err => {
        setDataLoader(dt => ({
          ...dt,
          loading: false,
          error: true,
          rd: false
        }));
      });
  };

  const fetchCats = async () => {
    setCatsLoading(true);
    setCatsError(false);
    props
      .categoryGet()
      .then(response => {
        setCats(response.data);
        setCatsLoading(false);
      })
      .catch(err => {
        setCatsError(true);
      });
  };

  const handleSubmitAdd = async (elements, catsSelected) => {
    let formData = new FormData();
    var selected_cats = [];
    for (var i = 0; i < catsSelected.length; i++) {
      if (catsSelected[i].checked) {
        selected_cats.push(catsSelected[i].value);
      }
    }

    if (selected_cats.length === 0) {
      props.setToastr(
        "Validation Error!",
        "Select at least one category",
        "error"
      );
      return;
    }

    if(elements.images.files.length > 0){
        for(var x = 0; x<elements.images.files.length; x++) {
            formData.append('images', elements.images.files[x])
        }
    } else {
        props.setToastr(
            "Validation Error!",
            "Select at least one image",
            "error"
          );
          return;
    }

    const business_name = elements.business_name.value;
    const email = elements.email.value;
    const phone = elements.phone.value;
    const website = elements.website.value;
    const address = elements.address.value;
    const description = elements.description.value;
    
    formData.append("business_name", business_name);
    formData.append("email", email);
    formData.append("phone", phone);
    formData.append("website", website);
    formData.append("address", address);
    formData.append("description", description);
    formData.append("categories", selected_cats);

    props
      .bkListingAdd(formData)
      .then(response => {
        fetchData();
        document.getElementById("close-modal").click();
        document.getElementById("form").reset();
      })
      .catch(err => {
        props.errorHandle(err);
      });
  };



  const handleSubmitEdit = async (
    elements,
    catsSelected,
    current_images,
    id
  ) => {
    let formData = new FormData();
    var selected_cats = [];
    for (var i = 0; i < catsSelected.length; i++) {
      if (catsSelected[i].checked) {
        selected_cats.push(catsSelected[i].value);
      }
    }

    if (selected_cats.length === 0) {
      props.setToastr(
        "Validation Error!",
        "Select at least one category",
        "error"
      );
      return;
    }

    if(current_images.length === 0 && elements.images.files.length === 0){
        props.setToastr(
            "Validation Error!",
            "Select at least one image",
            "error"
          );
          return;
    }

    if(elements.images.files.length > 0){
        for(var x = 0; x<elements.images.files.length; x++) {
            formData.append('images', elements.images.files[x])
        }
    }

    const business_name = elements.business_name.value;
    const email = elements.email.value;
    const phone = elements.phone.value;
    const website = elements.website.value;
    const address = elements.address.value;
    const description = elements.description.value;
    
    formData.append("id", id);
    formData.append("business_name", business_name);
    formData.append("email", email);
    formData.append("phone", phone);
    formData.append("website", website);
    formData.append("address", address);
    formData.append("description", description);
    formData.append("categories", selected_cats);
    formData.append("current_images", current_images);

    props
      .bkListingEdit(id, formData)
      .then(response => {
        fetchData();
        document.getElementById("close-modal").click();
        document.getElementById("form").reset();
      })
      .catch(err => {
        props.errorHandle(err);
      });
  };

  const deleteData = async Id => {
    const conf = window.confirm("Do you want to delete selected value?");
    if (!conf) return false;

    const response = await props.bkListingDelete(Id);
    setData(data.filter(data => data._id !== Id));
  };

  const handleOpenEdit = row => {
    setModalTitle(`Edit ${title}`);
    setEditMode(true);
    setEditData(row);
  };

  const handleDeletedata = id => {
    deleteData(id);
  };

  const clearFormData = () => {
    setEditMode(false);
    setEditData({});
  };

  useEffect(() => {
    fetchData();
    fetchCats();
  }, []);

  return (
    <Fragment>
      <div className="pb-3">
        <a
          href="javascript:void(0)"
          data-toggle="modal"
          data-target="#modalForm"
          onClick={() => setModalTitle(`Add New Business`)}
        >
          <i className="icon wb-plus" aria-hidden="true"></i>{" "}
          <span>Add Business</span>
        </a>
      </div>

      {dataLoader.loading && <div>Loading ...</div>}
      {dataLoader.error && <div>an error occured</div>}
      {dataLoader.rd && data.length === 0 && <div>No result found!</div>}

      {dataLoader.rd && data.length > 0 && (
        <table
          className="table table-bordered"
        >
          <thead>
            <tr>
              <th scope="col">Name</th>
              <th scope="col">Categories</th>
              <th scope="col" className="text-center">Views</th>
              <th className="text-center" scope="col">Action</th>
            </tr>
          </thead>
          <tbody>
            {data.map((row, index) => {
              return (
                <TableRow
                  key={index}
                  row={row}
                  handleOpenEdit={handleOpenEdit}
                  handleDeletedata={handleDeletedata}
                />
              );
            })}
          </tbody>
        </table>
      )}

      <div
        className="modal fade"
        id="modalForm"
        aria-hidden="true"
        aria-labelledby="modalForm"
        role="dialog"
        tabIndex="-1"
      >
        <div className="modal-dialog modal-lg">
          <div className="modal-content">
            <div className="modal-header">
              <h4 className="modal-title">{modalTitle}</h4>
              <button
                type="button"
                className="close"
                onClick={clearFormData}
                aria-hidden="true"
                data-dismiss="modal">×</button>
            </div>
            {editMode ? (
              <Edit
                handleSubmitEdit={handleSubmitEdit}
                editData={editData}
                clearFormData={clearFormData}
                cats={cats}
                catsLoading={catsLoading}
                catsError={catsError}
              />
            ) : (
              <Add
                handleSubmitAdd={handleSubmitAdd}
                cats={cats}
                catsLoading={catsLoading}
                catsError={catsError}
                clearFormData={clearFormData}
              />
            )}
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default withContext(Home);
