import React, { Fragment, useState, useEffect } from "react";
import { withContext } from "../../AppContext";
import TableRow from "./TableRow";
import Add from "./Add";
import Edit from "./Edit";

var pluralize = require("pluralize");

const Home = props => {
  const title = `Category`;
  const [data, setData] = useState([]);
  const [dataLoader, setDataLoader] = useState(false);

  const [modalTitle, setModalTitle] = useState("");
  const [editMode, setEditMode] = useState(false);
  const [editData, setEditData] = useState({});

  // Get main data
  const fetchData = async () => {
    setDataLoader(dt => ({ ...dt, loading: true, error: false, rd: false }));
    props
      .bkCategoryGet()
      .then(response => {
        setData(response.data);
        setDataLoader(dt => ({
          ...dt,
          loading: false,
          error: false,
          rd: true
        }));
      })
      .catch(err => {
        setDataLoader(dt => ({
          ...dt,
          loading: false,
          error: true,
          rd: false
        }));
      });
  };

  const handleSubmitAdd = async formData => {
    props
      .bkCategoryAdd(formData)
      .then(response => {
        fetchData();
        document.getElementById("close-modal").click();
        document.getElementById("form").reset();
      })
      .catch(err => {
        props.errorHandle(err);
      });
  };

  const handleSubmitEdit = async (formData, Id) => {
    props
      .bkCategoryEdit(Id, formData)
      .then(response => {
        setData(data.map(d => (d._id === Id ? response.data : d)));
        document.getElementById("close-modal").click();
        document.getElementById("form").reset();
      })
      .catch(err => {
        props.errorHandle(err);
      });
  };

  const deleteData = async Id => {
    const conf = window.confirm("Do you want to delete selected value?");
    if (!conf) return false;
    const response = await props.bkCategoryDelete(Id);
    setData(data.filter(data => data._id !== Id));
  };

  const handleOpenEdit = row => {
    setModalTitle(`Edit ${title}`);
    setEditMode(true);
    setEditData(row);
  };

  const handleDeletedata = id => {
    deleteData(id);
  };

  const clearFormData = () => {
    setEditMode(false);
    setEditData({});
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <Fragment>
      <div className="pb-4">
        <a
          href="javascript:void(0)"
          data-toggle="modal"
          data-target="#modalForm"
          onClick={() => setModalTitle(`Add New Category`)}
        >Add Category
        </a>
      </div>

      {dataLoader.loading && <div>Loading ...</div>}
      {dataLoader.error && <div>an error occured</div>}
      {dataLoader.rd && data.length === 0 && <div>No result found!</div>}

      {dataLoader.rd && data.length > 0 && (
        <table className="table table-bordered">
          <thead>
            <tr>
              <th scope="col">Name</th>
              <th scope="col" className="text-center">
                Status
              </th>
              <th scope="col" className="text-center">Action</th>
            </tr>
          </thead>
          <tbody>
            {data.map((row, index) => {
              return (
                <TableRow
                  key={index}
                  row={row}
                  handleOpenEdit={handleOpenEdit}
                  handleDeletedata={handleDeletedata}
                />
              );
            })}
          </tbody>
        </table>
      )}

      <div
        className="modal fade"
        id="modalForm"
        aria-hidden="true"
        aria-labelledby="modalForm"
        role="dialog"
        tabIndex="-1"
      >
        <div className="modal-dialog modal-simple">
          <div className="modal-content">
            <div className="modal-header">
              <button
                type="button"
                className="close"
                onClick={clearFormData}
                aria-hidden="true"
                data-dismiss="modal"
              >
                ×
              </button>
              <h4 className="modal-title">{modalTitle}</h4>
            </div>
            {editMode ? (
              <Edit
                handleSubmitEdit={handleSubmitEdit}
                editData={editData}
                clearFormData={clearFormData}
              />
            ) : (
              <Add handleSubmitAdd={handleSubmitAdd} />
            )}
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default withContext(Home);
