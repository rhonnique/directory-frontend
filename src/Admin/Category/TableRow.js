import React, { Fragment } from "react";

const TableRow = ({ row, handleOpenEdit, handleDeletedata }) => {

    return (
        <tr>

            <td>{row.name}</td>
            <td className="text-center">
                {row.status ? (
                    <span className="badge badge-outline badge-success">Active</span>
                ) : (
                        <span className="badge badge-outline badge-default">Inactive</span>
                    )}

            </td>
            <td className="text-center">
                <a onClick={() => handleOpenEdit(row)} href="javascript:void(0)" data-toggle="modal" data-target="#modalForm">Edit</a> | <a onClick={() => handleDeletedata(row._id)} href="javascript:void(0)">Delete</a>
            </td>
        </tr>

    );
}

export default TableRow;