import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from "react-router-dom";
import { AppContextProvider } from "./AppContext";
import ScrollToTop from 'react-router-scroll-top'
import App from "./App";

ReactDOM.render(
    <AppContextProvider>
        <Router>
        <ScrollToTop>
            <App/>
        </ScrollToTop>
        </Router>
    </AppContextProvider>,
    document.getElementById("root")
);